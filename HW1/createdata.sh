#!/bin/bash

#create directory for data
echo 'creating directory: donor_files'
mkdir donor_files
cd donor_files

echo 'in donor_files, making 10 timepoint files for each of 50 participants with 5 random numbers'
#cycle through 50 participants
for i in `seq 1 50`
do
    #cycle through 10 timepoints
    for n in `seq 1 10`
    do
	#make files with 5 random numbers
	a="data\n${RANDOM}\n${RANDOM}\n${RANDOM}\n${RANDOM}\n${RANDOM}"
	echo -e ${a} > donor${i}_tp${n}.txt
    done
done
