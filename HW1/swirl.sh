#!/bin/bash

for i in `seq 1 10`
do
    #Start spinning wheel clockwise, starting on '\'
    echo -ne '\\';
    sleep .2;
    echo -ne '\b|';
    sleep .2;
    echo -ne '\b/'
    sleep .2;
    echo -ne '\b-';
    sleep .2;
    echo -ne '\b\\';
    sleep .2;
    echo -ne '\b|';
    sleep .2;
    echo -ne '\b/';
    sleep .2;
    echo -ne '\b-';
    sleep 1;
    #uh oh, wheel breaks. pause
    echo -ne '\b ';
    sleep 1;
    #line grows
    echo -ne '\b-';
    sleep 0.5;
    echo -ne '\b--';
    sleep 0.3;
    echo -ne '\b\b---';
    sleep 0.1;
    echo -ne '\b\b\b----';
    sleep 0.1
    echo -ne '\b\b\b\b------';
    sleep 0.3;
    #pause, show '...'
    echo -ne '\b\b\b\b\b\b  ...  ';
    sleep 0.5
    #start double wheel! wheels roll to each other, then apart
    for i in `seq 1 5`
    do
	echo -ne '\b\b\b\b\b\b\b\b\b\b\b\\         |';
	sleep 0.5;
	echo -ne '\b\b\b\b\b\b\b\b\b\b\b |       \\ ';
	sleep 0.2;
	echo -ne '\b\b\b\b\b\b\b\b\b\b\b  /     - ';
	sleep 0.2;
	echo -ne '\b\b\b\b\b\b\b\b\b\b   -   / ';
	sleep 0.5;
	echo -ne '\b\b\b\b\b\b\b\b\b    \\ | ';
	sleep 1
	echo -ne '\b\b\b\b\b\b\b\b   -   / ';
	sleep 0.5;
	echo -ne '\b\b\b\b\b\b\b\b\b  /     - ';
	sleep 0.2;
	echo -ne '\b\b\b\b\b\b\b\b\b\b |       \\';
	sleep 0.2;
	echo -ne '\b\b\b\b\b\b\b\b\b\b\\         |';
	sleep 1
    done
    #delete final double wheel line before restart
    echo -ne '\b\b\b\b\b\b\b\b\b\b\b           '
    echo -ne '\n'
done
#new line
echo -ne '\n'
