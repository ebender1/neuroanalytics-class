#!/bin/bash

#go into donor_files directory
cd donor_files

#create a directory for each donor's 10 files
for i in `seq 1 50`
do
    mkdir donor${i}
    mv donor${i}_* donor${i}
done

#make an overall directory fakedata will all donors
mkdir fakedata
mv * fakedata

#make all files read only
chmod -R a-wx fakedata
