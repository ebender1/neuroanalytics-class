#!/bin/bash

#Make new donor files directory
echo "making donor files directory"
mkdir donorfiles2dir

#Cycle through 100 donors, create 10 files each
echo "initiating 10 files for each of 100 donors"
for donor in `seq 1 100`
do
    if [ ${donor} -lt 10 ]  
    then
	#puts 00 in front of donor number if donor number is one digit
	for tp in `seq 1 10`
	do
	    if [ ${tp} -lt 10 ]
	    then
		#puts 00 in front of tp number if tp number is one digit
		echo "data" > donor00${donor}_tp00${tp}.txt
		echo $RANDOM >> donor00${donor}_tp00${tp}.txt
		echo $RANDOM >> donor00${donor}_tp00${tp}.txt
		echo $RANDOM >> donor00${donor}_tp00${tp}.txt
		echo $RANDOM >> donor00${donor}_tp00${tp}.txt
		echo $RANDOM >> donor00${donor}_tp00${tp}.txt
	    else
		#puts 0 in front of tp number if tp number is two digits
		echo "data" > donor00${donor}_tp0${tp}.txt
		echo $RANDOM >> donor00${donor}_tp0${tp}.txt
		echo $RANDOM >> donor00${donor}_tp0${tp}.txt
		echo $RANDOM >> donor00${donor}_tp0${tp}.txt
		echo $RANDOM >> donor00${donor}_tp0${tp}.txt
		echo $RANDOM >> donor00${donor}_tp0${tp}.txt
	    fi
	done
    elif [ ${donor} -gt 9 ] && [ ${donor} -lt 100 ]
	 #puts 0 in front of donor number if donor number is one digit
	 for tp in `seq 1 10`
	 do
	     if [ ${tp} -lt 10 ]
	     then
		 #puts 00 in front of tp number if tp number is one digit
       		 echo "data" > donor0${donor}_tp00${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp00${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp00${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp00${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp00${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp00${tp}.txt
	     else
		 #puts 0 in front of tp number if tp number is two digits
	         echo "data" > donor0${donor}_tp0${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp0${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp0${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp0${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp0${tp}.txt
       		 echo $RANDOM >> donor0${donor}_tp0${tp}.txt
	     fi
	 done
    else
	#puts no zeroes in front of donor number if donor number is three digits
	for tp in `seq 1 10`
	do
	    if [ ${tp} -lt 10 ]
	    then
		#puts 00 in front of tp number if tp number is one digit
	      	echo "data" > donor${donor}_tp00${tp}.txt
	       	echo $RANDOM >> donor${donor}_tp00${tp}.txt
       		echo $RANDOM >> donor${donor}_tp00${tp}.txt
       		echo $RANDOM >> donor${donor}_tp00${tp}.txt
       		echo $RANDOM >> donor${donor}_tp00${tp}.txt
       		echo $RANDOM >> donor${donor}_tp00${tp}.txt
	    else
		#puts 0 in front of donor number if donor number is two digits
       		echo "data" > donor${donor}_tp0${tp}.txt
       		echo $RANDOM >> donor${donor}_tp0${tp}.txt
       		echo $RANDOM >> donor${donor}_tp0${tp}.txt
       		echo $RANDOM >> donor${donor}_tp0${tp}.txt
       		echo $RANDOM >> donor${donor}_tp0${tp}.txt
       		echo $RANDOM >> donor${donor}_tp0${tp}.txt
	    fi
	done
    fi
done

#yahoooooo
echo "yahooooo"
		
		
		    
		    
		
